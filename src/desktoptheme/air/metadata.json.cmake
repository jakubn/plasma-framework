{
    "KPlugin": {
        "Authors": [
            {
                "Email": "kde-artists@kde.org",
                "Name": "The Oxygen Project",
                "Name[ca@valencia]": "The Oxygen Project",
                "Name[ca]": "The Oxygen Project",
                "Name[es]": "El proyecto Oxígeno",
                "Name[eu]": "«Oxygen» proiektua",
                "Name[fr]": "Le projet « Oxygen »",
                "Name[it]": "Il progetto Oxygen",
                "Name[nl]": "Het project Oxygen",
                "Name[sl]": "Projekt kisik",
                "Name[tr]": "Oksijen Projesi",
                "Name[uk]": "Проєкт Oxygen",
                "Name[vi]": "Dự án Oxygen",
                "Name[x-test]": "xxThe Oxygen Projectxx"
            }
        ],
        "Category": "",
        "Description": "A breath of fresh air",
        "Description[ca@valencia]": "Una alenada d'aire fresc",
        "Description[ca]": "Una alenada d'aire fresc",
        "Description[es]": "Un soplo de aire fresco",
        "Description[eu]": "Haize freskoa arnastea",
        "Description[fr]": "Une bouffée d'air frais",
        "Description[it]": "Una ventata di aria fresca",
        "Description[nl]": "Een hap frisse lucht",
        "Description[sl]": "Vdih svežega zraka",
        "Description[tr]": "Temiz hava",
        "Description[uk]": "Ковток свіжого повітря",
        "Description[vi]": "Một luồng không khí trong lành",
        "Description[x-test]": "xxA breath of fresh airxx",
        "EnabledByDefault": true,
        "Id": "air",
        "License": "GPL",
        "Name": "Air",
        "Name[ca@valencia]": "Air",
        "Name[ca]": "Air",
        "Name[es]": "Aire",
        "Name[eu]": "Airea",
        "Name[fr]": "Air",
        "Name[it]": "Aria",
        "Name[nl]": "Air",
        "Name[sl]": "Zrak",
        "Name[tr]": "Hava",
        "Name[uk]": "Повітря",
        "Name[vi]": "Air",
        "Name[x-test]": "xxAirxx",
        "Version": "@KF_VERSION@",
        "Website": "https://plasma.kde.org"
    },
    "X-Plasma-API": "5.0"
}
